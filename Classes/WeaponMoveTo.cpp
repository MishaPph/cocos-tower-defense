#include "WeaponMoveTo.h"

void WeaponMoveTo::update(float t)
{
	if (m_pTarget)
	{
		if (t >= 1.0f) {
			m_pTarget->setPosition(m_endPosition);
		}
		else {
			auto point = m_previousPosition.lerp(m_endPosition, t);
			point.y += sin(t* M_PI)*m_yUp;
			m_pTarget->setPosition(point);
		}
	}
}

WeaponMoveTo* WeaponMoveTo::create(float duration, float yUp, const CCPoint& position)
{
	WeaponMoveTo *pRet = new WeaponMoveTo();
	pRet->initWithDuration(duration, position);
	pRet->m_yUp = yUp;
	pRet->autorelease();
	return pRet;
}

bool WeaponMoveTo::initWithDuration(float duration, const CCPoint& position)
{
	if (CCActionInterval::initWithDuration(duration))
	{
		m_endPosition = position;
		return true;
	}
	return false;
}

void WeaponMoveTo::startWithTarget(CCNode *pTarget)
{
	CCActionInterval::startWithTarget(pTarget);
	m_previousPosition = m_startPosition = pTarget->getPosition();
}