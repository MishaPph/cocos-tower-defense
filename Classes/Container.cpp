#include "Container.h"

static Container * s_sharedContainer = NULL;

 Container* Container::shared(void) {
	if (!s_sharedContainer)
	{
		s_sharedContainer = new Container();
	}
	return s_sharedContainer;
}

void Container::remove(Enemy *unit) {
	m_enemies.remove(unit);
}

void Container::push(Enemy *unit) {
	m_enemies.push_back(unit);
}

std::list<Enemy*> Container::getAllInRange(const cocos2d::CCPoint& pos, float range) {
	std::list<Enemy*> list;
	for (auto it = m_enemies.begin(); it != m_enemies.end(); ++it) {
		auto unit = (*it);
		if (unit->isDeath())
			continue;
		if (unit->getPosition().getDistance(pos) < range) {
			list.push_back(unit);
		}
	}
	return list;
}

Enemy* Container::getInRange(const cocos2d::CCPoint& pos, float range) {
	for (auto it = m_enemies.begin(); it != m_enemies.end(); ++it) {
		auto unit = (*it);
		if (unit->isDeath())
			continue;
		if (unit->getPosition().getDistance(pos) < range) {
			return unit;
		}
	}
	return nullptr;
}