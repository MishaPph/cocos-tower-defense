#pragma once
#include "cocos2d.h"
class Enemy;
class GameScene;
class Wave : public cocos2d::CCNode {
	GameScene * m_scene;
	int m_index;
	int lvl;
	float m_delay_lvl;
public:
	static Wave * create(GameScene * scene);
	void onEnemyFinish(Enemy* enemy);
	void nextLvl(float dt);
	void next(float dt);
	virtual bool init();
	virtual void onEnter() override;
	virtual void onExit() override;
};