
#ifndef __ASTAR__H
#define __ASTAR__H

#pragma once
#include "IntPair.h"
#include <list>
#include <unordered_map>

class Hasher
{
public:
	size_t operator() (IntPair const& key) const
	{
		return 65536 * key.x + key.y;
	}
};

class EqualFn
{
public:
	bool operator() (IntPair const& t1, IntPair const& t2) const
	{
		return t1.equals(t2);
	}
};


class AStarSearch
{
private:
	std::unordered_map<IntPair, IntPair, Hasher, EqualFn> m_cameFrom;
	std::unordered_map<IntPair, float, Hasher, EqualFn> m_costSoFar;
	bool FindPath(IntPair start, IntPair goal);
public:
	AStarSearch();
	~AStarSearch();
	bool GetPath(const IntPair& start, const IntPair& finish, std::list<IntPair> &list);
};

#endif // !__ASTAR__H
