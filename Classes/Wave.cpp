#include "Wave.h"
#include "Enemy.h"
#include "GameScene.h"
#include "Types.h"
#include "Factory.h"
USING_NS_CC;

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

typedef struct waveData
{
	float delay;
	EnemyId enemies[7];
} WaveData;

static const WaveData wave1 = { 0.6f,{ red, yellow, yellow, yellow, yellow, green, yellow } };
static const WaveData wave2 = { 0.5f,{ red, blue, blue, blue, yellow, green, yellow } };
static const WaveData wave3 = { 0.4f,{ red, green, yellow, green, green, green, yellow } };
static const WaveData wave4 = { 0.3f,{ red, blue, blue, yellow, yellow, green, green }};

static const WaveData waves[] = { wave1 , wave2, wave3, wave4 };

Wave* Wave::create(GameScene * scene)
{
	Wave *pWave = new Wave();
	pWave->m_scene = scene;
	if (pWave && pWave->init())
	{
		pWave->autorelease();
		return pWave;
	}
	CC_SAFE_DELETE(pWave);
	return NULL;
}

bool Wave::init() {
	if (!CCNode::init())
	{
		return false;
	}
	lvl = -1;
	m_delay_lvl = 20;
	this->schedule(schedule_selector(Wave::nextLvl), 5.0f);
	return true;
}

void Wave::nextLvl(float dt) {
	lvl++;
	m_index = 0;
	this->unschedule(schedule_selector(Wave::nextLvl));
	if(lvl >= 4) {
		m_delay_lvl *= 0.7f;
		lvl = 0;
	}
	this->schedule(schedule_selector(Wave::next), waves[lvl].delay);
}

void Wave::next(float dt) {
	Enemy* enemy = Factory::createEnemy(waves[lvl].enemies[m_index]);
	enemy->setCallback(this, (SEL_CallFuncEnemy)(&Wave::onEnemyFinish));
	m_scene->addChild(enemy);
	m_index++;
	if (m_index >= 7) {
		this->unschedule(schedule_selector(Wave::next));
		this->schedule(schedule_selector(Wave::nextLvl), m_delay_lvl);
	}
}

void Wave::onEnemyFinish(Enemy* enemy) {
	if (enemy->isFinish()) {
		m_scene->addEnemyFinish(enemy->getPosition());
	}
	else {
		m_scene->addMoney(enemy->getPosition(), enemy->getData()->reward);
	}
}

void Wave::onEnter()
{
	CCNode::onEnter(); //call base class onEnter()
	scheduleUpdate();
}

void Wave::onExit()
{
	CCNode::onExit(); //call base class onExit()
	unscheduleUpdate();
}