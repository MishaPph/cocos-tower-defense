#ifndef __TYPES_H__
#define __TYPES_H__

enum TowerId { arrow, bomb, mage, ice };

typedef struct towerData
{
	int price;
	TowerId tag;
	int damage;
	int range;
	float hitSpeed;
} TowerData;

static const char * TowerIdStrings[] = { "arrow", "bomb", "mage", "ice" };
static const char * EnemyIdStrings[] = { "blue", "green", "yellow", "red" };

enum EnemyId { blue, green, yellow, red};

typedef struct enemyData
{
	EnemyId tag;
	int hp;
	float speed;
	int reward;
} EnemyData;


#endif //__TYPES_H__
