#include "Enemy.h"
#include "Map.h"
#include "Container.h"

USING_NS_CC;

Enemy* Enemy::create(EnemyData * data)
{
	Enemy *pEnemy = new Enemy();
	pEnemy->setUserData(data);
	pEnemy->hp = data->hp;
	
	CCString* name = CCString::createWithFormat("enemy_%s.png", EnemyIdStrings[data->tag]);
	if (pEnemy && pEnemy->initWithFile(name->getCString()))
	{
		pEnemy->autorelease();
		pEnemy->init();
		return pEnemy;
	}
	CC_SAFE_DELETE(pEnemy);
	return NULL;
}

void Enemy::setCallback(cocos2d::CCObject* target, SEL_CallFuncEnemy selector) {
	m_callFunc = selector;
	m_pListener = target;
}

bool Enemy::init(void) {

	m_level = 1;
	m_finish = false;
	auto vect = Map::sharedMap()->getSpawnPoints();
	path = Map::sharedMap()->getPath(vect[rand() % vect.size()]);
	setPosition(Map::sharedMap()->convertToCCP(path.front()));

	CCSprite* bg = CCSprite::create("hp_bar.png");
	bg->setPosition(ccp(0, getContentSize().height));
	bg->setColor(cocos2d::ccRED);
	bg->setAnchorPoint(ccp(0, 0));
	addChild(bg, 0);
	m_lifebar = CCSprite::create("hp_bar.png");
	m_lifebar->setAnchorPoint(ccp(0, 0));
	m_lifebar->setPosition(ccp(0, getContentSize().height));
	m_lifebar->setColor(cocos2d::ccGREEN);
	addChild(m_lifebar, 1);
	Container::shared()->push(this);
	return true;
}

Enemy::Enemy()
{
	m_lifebar = nullptr;
	m_freeze = 0;
}

Enemy::~Enemy()
{
	CCLOG("~BaseEnemy");
}

void Enemy::damaged(int value) {
	hp -= value;
	if (isDeath()) {
		callDeath();
	}
	else {
		m_lifebar->setScaleX((float)hp/getData()->hp);
	}
}

void Enemy::callDeath() {
	if (m_pListener && m_callFunc)
	{
		(m_pListener->*m_callFunc)(this);
	}
	Container::shared()->remove(this);
	this->removeFromParent();
	CC_SAFE_RELEASE(this);
}

bool Enemy::isFinish() {
	return m_finish;
}

bool Enemy::isDeath() {
	return hp <= 0;
}

void Enemy::freeze(float time) {
	m_freeze = time;
}

EnemyData * Enemy::getData() {
	return (EnemyData *)getUserData();
}

void Enemy::onEnter()
{
	CCSprite::onEnter(); //call base class onEnter()
	scheduleUpdate();
}

void Enemy::onExit()
{
	CCSprite::onExit(); //call base class onExit()
	unscheduleUpdate();
}

void Enemy::update(float dt) 
{
	if (path.size() == 0) {
		m_finish = true;
		callDeath();
		return;
	}
	auto next = Map::sharedMap()->convertToCCP(path.front());
	if (getPosition().getDistance(next) < getData()->speed*2) {
		path.pop_front();
		if (path.size() == 0) {
			m_finish = true;
			callDeath();
			return;
		}
		next = Map::sharedMap()->convertToCCP(path.front());
	}
	m_freeze -= dt;
	auto speed = (m_freeze > 0) ? getData()->speed*0.5f : getData()->speed;
	auto dir = getPosition() - next;
	setPosition(getPosition() - dir.normalize()*speed);
}