#pragma once
#include "IntPair.h"

using namespace std;
template <class T> class Grid
{
	T** m_cells;
	int m_sizeX, m_sizeY;
public:
	Grid(int sizeX, int sizeY) {
		m_sizeX = sizeX;
		m_sizeY = sizeY;
		m_cells = new T*[sizeX];
		for (int i = 0; i < sizeX; ++i)
			m_cells[i] = new T[sizeY];
	}
	~Grid(void) {
		for (int i = 0; i < m_sizeY; ++i) {
			delete[] m_cells[i];
		}
		delete[] m_cells;
	}
	T getCell(int x, int y);
	void setCell(int x, int y, T value);
};

template <class T> T Grid<T>::getCell(int x, int y)
{
	if (x < 0 || x >= m_sizeX)
		return T(0);
	if (y < 0 || y >= m_sizeY)
		return T(0);
	return m_cells[x][y];
}

template <class T> void Grid<T>::setCell(int x, int y, T value)
{
	if (x < 0 || x >= m_sizeX)
		return;
	if (y < 0 || y >= m_sizeY)
		return;

	m_cells[x][y] = value;
}
