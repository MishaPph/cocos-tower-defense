#ifndef __WEAPON_H__
#define __WEAPON_H__


#pragma once
#include "cocos2d.h"
#include "Types.h"

#pragma once
class Enemy;
class Weapon : public cocos2d::CCSprite
{
	TowerData * m_data;
	cocos2d::CCPoint toPoint;
	int m_lvl;
public:
	Weapon();
	~Weapon();

	static Weapon* create(TowerData * data, Enemy* target);
	void callback(cocos2d::CCNode* sender, void* data);
};

#endif