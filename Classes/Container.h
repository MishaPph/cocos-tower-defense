#ifndef __CONTAINER_H__
#define __CONTAINER_H__

#include<list>
#include "Enemy.h"

#pragma once
class Container
{
	std::list<Enemy*> m_enemies;
public:
	static Container* shared(void);

	void push(Enemy *unit);
	void remove(Enemy *unit);
	std::list<Enemy*> getAllInRange(const cocos2d::CCPoint& pos, float range);
	Enemy* getInRange(const cocos2d::CCPoint& pos, float range);
};

#endif // !__CONTAINER_H__