#include "IntPair.h"

IntPair::~IntPair()
{
}

IntPair::IntPair(void) : x(0), y(0)
{
}

IntPair::IntPair(int x, int y) : x(x), y(y)
{
}

IntPair& IntPair::operator= (const IntPair& other)
{
	setPoint(other.x, other.y);
	return *this;
}


IntPair IntPair::operator+(const IntPair& right) const
{
	return IntPair(this->x + right.x, this->y + right.y);
}

IntPair IntPair::operator-(const IntPair& right) const
{
	return IntPair(this->x - right.x, this->y - right.y);
}

IntPair IntPair::operator-() const
{
	return IntPair(-x, -y);
}

IntPair IntPair::operator*(int a) const
{
	return IntPair(this->x * a, this->y * a);
}

IntPair IntPair::operator/(int a) const
{
	return IntPair(this->x / a, this->y / a);
}

void IntPair::setPoint(int x, int y)
{
	this->x = x;
	this->y = y;
}

bool IntPair::equals(const IntPair& target) const
{
	return this->x == target.x && this->y == target.y;
}
