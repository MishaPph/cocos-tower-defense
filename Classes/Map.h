#ifndef __MAP_H__
#define __MAP_H__
#include "cocos2d.h"
#include "Grid.h"
#include "AStarSearch.h"

using namespace std;

class Map: public cocos2d::CCObject
{
	list<cocos2d::CCPoint> m_pointTowers;
	vector<IntPair> m_pointSpawn;
	AStarSearch* m_search;
	Grid<bool>* road_grid;
	IntPair to;
	float coefX, coefY;
public:
	Map();
	virtual ~Map(void);

	virtual bool init(void);

	bool loadMap(const char * file_name);
	const list<cocos2d::CCPoint> getTowerPoints();
	const std::vector<IntPair> getSpawnPoints();
	static Map* sharedMap(void);
	bool IsPassable(const IntPair &pos);
	const std::list<IntPair> Map::getPath(const IntPair &from);
	const cocos2d::CCPoint convertToCCP(const IntPair &pos);
};

#endif
