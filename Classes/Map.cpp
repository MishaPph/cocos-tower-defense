#include "Map.h"
#include "TowerPoint.h"

static Map *s_SharedMap = NULL;
Map* Map::sharedMap(void)
{
	if (!s_SharedMap)
	{
		s_SharedMap = new Map();
		s_SharedMap->init();
	}
	return s_SharedMap;
}

bool Map::init(void)
{
	return true;
}

Map::Map()
{
	m_search = new AStarSearch();
}

Map::~Map(void)
{
	CC_SAFE_DELETE(m_search);
	CC_SAFE_DELETE(road_grid);
}

bool Map::loadMap(const char * file_name)
{
	CCImage*  image = new CCImage();
	if (image->initWithImageFile(file_name)) {
		int count_colors = (image->hasAlpha()) ? 4 : 3;
		unsigned char* cImages = image->getData();
		coefX = CCDirector::sharedDirector()->getVisibleSize().width / image->getWidth();
		coefY = CCDirector::sharedDirector()->getVisibleSize().height / image->getHeight();
		//CC_SAFE_DELETE(road_grid);
		road_grid = new Grid<bool>(image->getWidth(), image->getHeight());
		int w = image->getWidth();
		int h = image->getHeight();
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				auto r = cImages[i*h*count_colors + j*count_colors + 0];
				auto g = cImages[i*h*count_colors + j*count_colors + 1];
				auto b = cImages[i*h*count_colors + j*count_colors + 2];
				if (g == 255) {
					m_pointTowers.push_front(ccp(j*coefX, i*coefY));
				}
				if (b == 255) {
					//m_pointTowers.push_front(ccp(j*coefX, i*coefY));
					road_grid->setCell(i, j, true);
					if (j == 0) {
						m_pointSpawn.push_back(IntPair(i,j));
					}
					if (j == h - 1) {
						to = IntPair(i, j);
					}
				}
				else {
					road_grid->setCell(i, j, false);
				}
			}
		}
	}
	return true;
}
const cocos2d::CCPoint Map::convertToCCP(const IntPair &pos) {
	return ccp(pos.y * coefX, pos.x * coefY);
}

bool Map::IsPassable(const IntPair &pos) {
	return road_grid->getCell(pos.x, pos.y);
}

const std::list<cocos2d::CCPoint> Map::getTowerPoints() {
	return m_pointTowers;
}

const std::vector<IntPair>  Map::getSpawnPoints() {
	return m_pointSpawn;
}

const std::list<IntPair> Map::getPath(const IntPair &from) {
	std::list<IntPair> list;
	if (m_search->GetPath(to, from, list)) {
		for (auto it = list.begin(); it != list.end(); ++it) {
			//CCLog("%d %d", it->x, it->y);
		}
	}
	return list;
}