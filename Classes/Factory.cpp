#include "Factory.h"
#include "Enemy.h"
#include "Weapon.h"
#include "Tower.h"

static TowerData arrowTower = { 100, arrow, 12, 50, 0.25f };
static TowerData bombTower = { 100,  bomb, 8, 70, 1.0f };
static TowerData mageTower = { 100,  mage, 25, 110, 1.5f };
static TowerData iceTower = { 100, ice, 10, 80, 0.5f };

static EnemyData redEnemy = { red, 32, 1.0f, 10 };
static EnemyData yellowEnemy = { yellow, 150, 1.0f, 50 };
static EnemyData blueEnemy = { blue, 500, 0.3f, 150 };
static EnemyData greenEnemy = { green, 60, 2.0f, 200 };

Enemy* Factory::createEnemy(EnemyId id) {
	switch (id)
	{
	case red:
		return Enemy::create(&redEnemy);
	case yellow:
		return Enemy::create(&yellowEnemy);
	case blue:
		return Enemy::create(&blueEnemy);
	case green:
		return Enemy::create(&greenEnemy);
	default:
		break;
	}
	return Enemy::create(&redEnemy);
}

Weapon* Factory::createWeapon(Tower* tower) {
	auto weapon = Weapon::create(&Factory::getTowerData(tower->getId()), tower->getTarget());
	tower->getParent()->addChild(weapon);
	weapon->setPosition(tower->getPosition());
	auto lookVect = tower->getPosition() - tower->getTarget()->getPosition();
	weapon->setRotation(CC_RADIANS_TO_DEGREES(-lookVect.getAngle()));
	return weapon;
}

TowerData& Factory::getTowerData(TowerId id) {
	switch (id)
	{
	case arrow:
		return arrowTower;
	case bomb:
		return bombTower;
	case mage:
		return mageTower;
	case ice:
		return iceTower;
	default:
		break;
	}
	return arrowTower;
}