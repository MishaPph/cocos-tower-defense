#include "BattleUI.h"
#include "GameScene.h"
#include "AppMacros.h"
#include "Factory.h"

USING_NS_CC;

bool BattleUI::init() {
	if (!CCLayer::init())
	{
		return false;
	}
	this->m_bTouchEnabled = true;
	setTouchEnabled(true);

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	m_MoneyLabel = CCLabelTTF::create("10900", "Arial", TITLE_FONT_SIZE);
	addChild(m_MoneyLabel, 1);
	m_MoneyLabel->setPosition(ccp(m_MoneyLabel->getContentSize().width, visibleSize.height - m_MoneyLabel->getContentSize().height/2));
	CCSprite* gold = CCSprite::create("gold.png");
	gold->setPosition(ccp(-gold->getContentSize().width/2, gold->getContentSize().height/2));
	m_MoneyLabel->addChild(gold);


	m_DeathLabel = CCLabelTTF::create("18", "Arial", TITLE_FONT_SIZE);
	addChild(m_DeathLabel, 1);
	m_DeathLabel->setPosition(ccp(m_DeathLabel->getContentSize().width + m_MoneyLabel->getPosition().x + m_MoneyLabel->getContentSize().width,
		visibleSize.height - m_DeathLabel->getContentSize().height / 2));
	CCSprite* enemy = CCSprite::create("enemy.png");
	enemy->setPosition(ccp(-enemy->getContentSize().width / 2, m_DeathLabel->getContentSize().height / 2));
	m_DeathLabel->addChild(enemy);

	CCMenuItemImage *pCloseItem = CCMenuItemImage::create("select_back.png", "select_back.png", this, menu_selector(BattleUI::changePause));

	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width / 2,
		origin.y + pCloseItem->getContentSize().height / 2));

	CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
	pMenu->setPosition(CCPointZero);
	addChild(pMenu, 1);

	createPause();
	createGroupTowers();
	createChangeTower();
	return true;
}

void BattleUI::setMoney(int count) {
	CCString* buficon = CCString::createWithFormat("%d", count);
	m_MoneyLabel->setString(buficon->getCString());
}

void BattleUI::setFinishEnemy(int count) {
	CCString* buficon = CCString::createWithFormat("%d", count);
	m_DeathLabel->setString(buficon->getCString());
}

void BattleUI::setMoney(const cocos2d::CCPoint &from, int count) {
	CCSprite * coin = CCSprite::create("gold.png");
	coin->setScale(0.5f);
	coin->setTag(count);
	addChild(coin, 50);
	coin->setPosition(from);
	CCFiniteTimeAction*  action = CCSequence::create(
		CCMoveTo::create(0.6f, m_MoneyLabel->getPosition()),
		CCCallFuncN::create(this, callfuncN_selector(BattleUI::finishCoins)),
		NULL);
	coin->runAction(action);
}

void BattleUI::finishCoins(CCNode* node) {
	CCString* buficon = CCString::createWithFormat("%d", node->getTag());
	node->removeFromParentAndCleanup(true);
	m_MoneyLabel->setString(buficon->getCString());
}

void BattleUI::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
	m_GroundMenu->setVisible(false);
	m_ChageMenu->setVisible(false);
}

CCMenuItemImage* BattleUI::getTowerIcon(TowerData &towerData) {
	CCString* buficon = CCString::createWithFormat("icon_%s.png", TowerIdStrings[towerData.tag]);
	buficon->autorelease();
	CCMenuItemImage *icon = CCMenuItemImage::create(
		buficon->getCString(),
		buficon->getCString(),
		this,
		menu_selector(BattleUI::createTower));

	CCString* string = CCString::createWithFormat("%d", towerData.price);
	string->autorelease();
	CCLabelTTF* pLabel = CCLabelTTF::create(string->getCString(), "Arial", 14);
	pLabel->setPosition(ccp(icon->getContentSize().width / 2, 0));
	icon->addChild(pLabel, 15);
	icon->setUserData(&towerData);

	return icon;
}

void BattleUI::createGroupTowers() {

	auto arrow = getTowerIcon(Factory::getTowerData(TowerId::arrow));
	arrow->setPosition(ccp(-30, 30));

	auto bomb = getTowerIcon(Factory::getTowerData(TowerId::bomb));
	bomb->setPosition(ccp(30, 30));

	auto mage = getTowerIcon(Factory::getTowerData(TowerId::mage));
	mage->setPosition(ccp(-30, -30));

	auto ice = getTowerIcon(Factory::getTowerData(TowerId::ice));
	ice->setPosition(ccp(30, -30));

	m_GroundMenu = CCMenu::create(arrow, bomb, mage, ice, nullptr);

	this->addChild(m_GroundMenu, 2);
	m_GroundMenu->setVisible(false);
}

void BattleUI::createChangeTower()
{
	CCMenuItemImage *upgrade = CCMenuItemImage::create(
		"icon_upgrade.png",
		"icon_upgrade.png",
		this,
		menu_selector(BattleUI::upgradeTower));

	CCMenuItemImage *destroy = CCMenuItemImage::create(
		"icon_destroy.png",
		"icon_destroy.png",
		this,
		menu_selector(BattleUI::destroyTower));

	upgrade->setPosition(ccp(0, 30));
	upgrade->setTag(1);
	CCLabelTTF* pLabel = CCLabelTTF::create("0", "Arial", 14);
	pLabel->setPosition(ccp(upgrade->getContentSize().width / 2, 0));
	pLabel->setTag(15);
	upgrade->addChild(pLabel, 1, 15);

	destroy->setPosition(ccp(0, -30));

	m_ChageMenu = CCMenu::create(upgrade, destroy, nullptr);
	this->addChild(m_ChageMenu, 100);
	m_ChageMenu->setVisible(false);
}

void BattleUI::showUpgrade(const CCPoint &position, int cost, bool available) {
	m_GroundMenu->setVisible(false);
	m_ChageMenu->setVisible(true);
	m_ChageMenu->setPosition(position);

	CCString* buficon = CCString::createWithFormat("%d", cost);
	auto arr = m_ChageMenu->getChildByTag(1)->getChildrenCount();
	auto upgrade = (CCMenuItem *)m_ChageMenu->getChildByTag(1);
	auto label = (CCLabelTTF *)(upgrade->getChildByTag(15));
	label->setString(buficon->getCString());

	if (available) {
		upgrade->setColor(ccWHITE);
		upgrade->setEnabled(true);
	}
	else {
		upgrade->setColor(ccGRAY);
		upgrade->setEnabled(false);
	}
}

void BattleUI::createPause() {

	CCMenuItemLabel * resume = CCMenuItemLabel::create(CCLabelTTF::create("Continue", "Arial", 34), this, 
		menu_selector(BattleUI::changePause));

	CCMenuItemLabel * close = CCMenuItemLabel::create(CCLabelTTF::create("Exit", "Arial", 34), this,
		menu_selector(GameScene::menuCloseCallback));

	m_PauseMenu = CCMenu::create(resume, close, nullptr);
	close->setPosition(ccp(0, -close->getContentSize().height*2));
	this->addChild(m_PauseMenu, 100);

	m_PauseMenu->setVisible(false);
}

void BattleUI::changePause(CCObject* pSender) {
	if (m_PauseMenu->isVisible()) {
		m_PauseMenu->setVisible(false);
		CCDirector::sharedDirector()->resume();
	}
	else {
		m_PauseMenu->setVisible(true);
		CCDirector::sharedDirector()->pause();
	}
}

void BattleUI::showEnd(bool win) {

}

void BattleUI::showCreateTower(const CCPoint &position, int money) {
	m_ChageMenu->setVisible(false);
	m_GroundMenu->setVisible(true);
	m_GroundMenu->setPosition(position);
	auto arr = m_GroundMenu->getChildren();
	for (int i = 0; i < arr->count(); i++) {
		auto menu = (CCMenuItemImage*)(arr->objectAtIndex(i));
		auto data = (towerData *)menu->getUserData();
		if (data->price <= money) {
			menu->setColor(ccWHITE);
			menu->setEnabled(true);
		} else {
			menu->setColor(ccGRAY);
			menu->setEnabled(false);
		}
	}
}

void BattleUI::createTower(CCObject* pSender) {
	m_ChageMenu->setVisible(false);
	m_GroundMenu->setVisible(false);
	((GameScene *)m_scene)->createTower(pSender);
}

void BattleUI::upgradeTower(CCObject* pSender) {
	m_ChageMenu->setVisible(false);
	m_GroundMenu->setVisible(false);
	((GameScene *)m_scene)->upgradeTower(pSender);
}

void BattleUI::destroyTower(CCObject* pSender) {
	m_ChageMenu->setVisible(false);
	m_GroundMenu->setVisible(false);
	((GameScene *)m_scene)->destroyTower(pSender);
}