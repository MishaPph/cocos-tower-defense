#include "Weapon.h"
#include "Enemy.h"
#include "WeaponMoveTo.h"
#include "Container.h"

USING_NS_CC;

Weapon::Weapon()
{
}

Weapon::~Weapon()
{
	CCLOG("~Weapon");
}

Weapon* Weapon::create(TowerData * data,  Enemy* target)
{
	Weapon *pWeapon = new Weapon();
	pWeapon->m_data = data;
	char * buficon = new char[28];
	snprintf(buficon, 28, "weapon_%s.png", TowerIdStrings[data->tag]);
	if (pWeapon && pWeapon->initWithFile(buficon))
	{
		pWeapon->autorelease();
		pWeapon->m_lvl = 1;
		CCFiniteTimeAction*  action;
		switch (data->tag)
		{
		case TowerId::arrow:
			action = CCSequence::create(
				WeaponMoveTo::create(0.2f, 15, target->getPosition()),
				CCCallFuncND::create(pWeapon, callfuncND_selector(Weapon::callback), (void *)target),
				NULL);
			break;
		case TowerId::bomb:
			action = CCSequence::create(
				WeaponMoveTo::create(0.4f, 50, target->getPosition()),
				CCCallFuncND::create(pWeapon, callfuncND_selector(Weapon::callback), (void *)target),
				NULL);
			break;
		case TowerId::mage:
			action = CCSequence::create(
				CCMoveTo::create(0.2f, target->getPosition()),
				CCCallFuncND::create(pWeapon, callfuncND_selector(Weapon::callback), (void *)target),
				NULL);
			break;
		case TowerId::ice:
			action = CCSequence::create(
				CCMoveTo::create(0.1f, target->getPosition()),
				CCCallFuncND::create(pWeapon, callfuncND_selector(Weapon::callback), (void *)target),
				NULL);
			break;
		default:
			break;
		}
		pWeapon->runAction(action);
		return pWeapon;
	}
	CC_SAFE_DELETE(pWeapon);
	return NULL;
}

void Weapon::callback(CCNode* sender, void* data)
{
	if (m_data->tag == TowerId::bomb) {
		auto list = Container::shared()->getAllInRange(getPosition(), m_data->range);
		for each (Enemy* unit in list)
		{
			unit->damaged(m_data->damage);
		}
	}
	else {
		auto enemy = (Enemy*)data;
		if (enemy != nullptr && !enemy->isDeath()) {
			enemy->damaged(m_data->damage);
			if (m_data->tag == TowerId::ice) {
				enemy->freeze(2.0f);
			}
		}
	}
	removeFromParentAndCleanup(true);
	
}