#pragma once
#include "Types.h"
#include "Enemy.h"
class Enemy;
class Weapon;
class Tower;

class Factory {
public:
	static Enemy* createEnemy(EnemyId id);
	static TowerData& getTowerData(TowerId id);
	static Weapon* createWeapon(Tower* tower);
};