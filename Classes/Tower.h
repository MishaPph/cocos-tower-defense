#ifndef __BASE_TOWER_H__
#define __BASE_TOWER_H__

#pragma once
#include "cocos2d.h"
#include "Types.h"

class Enemy;

class Tower : public cocos2d::CCSprite
{
protected:
	int m_level;
	TowerData * m_data;
	Enemy* target;
	float m_lastHit;
public:
	Tower();
	~Tower();
	static Tower* create(TowerData * data);
	TowerId getId();
	int getRange();
	Enemy* getTarget();
	int upgradePrice();
	int destroyPrice();
	void upgrade();

	virtual void onEnter() override;
	virtual void onExit() override;
	virtual void update(float dt) override;
};
#endif // __BASE_TOWER_H__