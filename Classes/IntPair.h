#ifndef __INTPAIR_H__ 
#define __INTPAIR_H__
#include <unordered_map>

#pragma once

class IntPair
{
public:
	int x;
	int y;
	IntPair();
	~IntPair();

	IntPair(int x, int y);
	IntPair& operator= (const IntPair& other);
	IntPair operator+(const IntPair& right) const;
	IntPair operator-(const IntPair& right) const;
	IntPair operator-() const;
	IntPair operator*(int a) const;
	IntPair operator/(int a) const;

	bool operator==(const IntPair &other) const {
		return this->equals(other);
	}

	bool operator!=(const IntPair &other) const {
		return !this->equals(other);
	}

	void setPoint(int x, int y);
	bool equals(const IntPair& target) const;

	inline float getLengthSq() const {
		return x*x + y*y;
	};
	inline float getDistanceSq(const IntPair& other) const {
		return (*this - other).getLengthSq();
	};

	std::size_t operator()(const IntPair& k) const
	{
		return ((std::hash<int>()(k.x) ^ (std::hash<int>()(k.y) << 1)) >> 1);
	}
};

#endif // !__INTPAIR_H__ 