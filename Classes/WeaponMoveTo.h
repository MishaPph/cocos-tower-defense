#ifndef __WEAPON_MOVE_TO_H__
#define __WEAPON_MOVE_TO_H__

#include "cocos2d.h"
#pragma once
using namespace cocos2d;

class WeaponMoveTo : public CCMoveBy
{
public:
	bool initWithDuration(float duration, const CCPoint& position);
public:
	/** creates the action */
	static WeaponMoveTo* create(float duration, float yUp, const CCPoint& position);
	virtual void update(float time);
	virtual void startWithTarget(CCNode *pTarget);
protected:
	CCPoint m_endPosition;
	float m_yUp;
};
#endif // ! __WEAPON_MOVE_TO_H__