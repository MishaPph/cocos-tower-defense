#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Types.h"

class TowerPoint;
class Map;
class BattleUI;

class GameScene : public cocos2d::CCLayer
{
	TowerPoint * m_activePoint;
	int m_Money;
	int m_enemyCount;
	BattleUI *m_ui;
public:

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
	virtual void onEnter() override;
    // a selector callback
    void menuCloseCallback(CCObject* pSender);

    // implement the "static node()" method manually
    CREATE_FUNC(GameScene);

	void addTowerPoint(cocos2d::CCPoint &point);
	void createTower(CCObject* pSender);
	void upgradeTower(CCObject* pSender);
	void destroyTower(CCObject* pSender);

#pragma region Money
	bool canBuy(int count);
	void addMoney(int count);
	void addMoney(const cocos2d::CCPoint &from, int count);
#pragma endregion
	void addEnemyFinish(const cocos2d::CCPoint &from);

	void pointClickCallback(CCObject* pSender);
};

#endif // __GAME_SCENE_H__
 