#include "TowerPoint.h"

TowerPoint * TowerPoint::create(CCNode *rec, CCObject* target, SEL_MenuHandler selector)
{
	TowerPoint *pRet = new TowerPoint();
	pRet->m_Tower = nullptr;
	pRet->initWithTarget(target, selector);
	pRet->autorelease();
	pRet->setContentSize(rec->getContentSize());
	pRet->addChild(rec);
	pRet->m_node = rec;
	rec->setAnchorPoint(ccp(0, 0));
	return pRet;
}
