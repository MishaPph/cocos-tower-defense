
#include "AStarSearch.h"
#include "Map.h"
#include <queue>

struct PriorityStruct
{
	IntPair pair;
	float priority;
	PriorityStruct(IntPair p, int prior) {
		pair = p;
		priority = prior;
	}
};

class comparator
{
public:
	bool operator()(const PriorityStruct& a, const PriorityStruct& b)
	{
		return a.priority > b.priority;
	}
};

AStarSearch::AStarSearch() {
}

AStarSearch::~AStarSearch() {
}

bool AStarSearch::FindPath(IntPair start, IntPair goal)
{
	auto frontier = priority_queue<PriorityStruct, vector<PriorityStruct>, comparator>();
	frontier.push(PriorityStruct(start, 0));
	m_cameFrom.clear();
	m_costSoFar.clear();

	m_cameFrom[start] = start;
	m_costSoFar[start] = 0;

	
	int k = 0;
	while (!frontier.empty() && k < 300)
	{
		auto current = frontier.top().pair;
		float currentCost = m_costSoFar[current];

		frontier.pop();
		if (current.equals(goal))
		{
			break;
		}

		for (int i = -1; i < 2; i++)
		{
			for (int j = -1; j < 2; j++)
			{
				if (i == 0 && j == 0)
					continue;
				//road_grid
				auto next = IntPair(current.x + i, current.y + j);
				if (!Map::sharedMap()->IsPassable(next))
					continue;
				auto newCost = currentCost + ccp(i, j).getLength();
				if (m_costSoFar.find(next) != m_costSoFar.end() && newCost >= m_costSoFar[next]) continue;
				m_costSoFar[next] = newCost;
				frontier.push(PriorityStruct(next,  newCost));
				m_cameFrom[next] = current; 
			}
		}
		k++;
	}
	CCAssert(k < 300, "Error out");
	return true;
}

bool AStarSearch::GetPath(const IntPair& start, const IntPair& finish,  std::list<IntPair> &list)
{
	if (start.getDistanceSq(finish) <= 1)
	{
		list.push_back(finish);
		return true;
	}

	if (!FindPath(finish, start))
	{
		CCLOG("Path not found");
		return false;
	}

	auto pstart = start;
	int k = 0;
	while (finish != pstart && k < 200)
	{
		pstart = m_cameFrom[pstart];
		list.push_front(pstart);
		k++;
	}
	CCAssert(k < 200, "Error out");
	if (list.size() <= 1) {
		list.clear();
		list.push_back(finish);
	}
	else {
		//list.remove(0);
	}
	list.push_back(start);
	//list.reverse();
	return true;
}