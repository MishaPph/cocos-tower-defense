#include "Tower.h"
#include "Types.h"
#include "Enemy.h"
#include "Container.h"
#include "Weapon.h"
#include "Factory.h"

USING_NS_CC;

Tower* Tower::create(TowerData * data)
{
	Tower *pobSprite = new Tower();
	pobSprite->m_data = data;
	CCString* name = CCString::createWithFormat("tower_%s.png", TowerIdStrings[data->tag]);
	if (pobSprite && pobSprite->initWithFile(name->getCString()))
	{
		pobSprite->autorelease();
		return pobSprite;
	}
	CC_SAFE_DELETE(pobSprite);
	return NULL;
}

Tower::Tower()
{
	m_level = 1;
	m_lastHit = 0;
	target = nullptr;
}

Tower::~Tower()
{
}

void Tower::onEnter()
{
	CCSprite::onEnter(); //call base class onEnter()
	scheduleUpdate();
}

void Tower::onExit()
{
	CCSprite::onExit(); //call base class onExit()
	unscheduleUpdate();
}

void Tower::update(float dt) {
	//CCLOG("update");
	if (m_lastHit < m_data->hitSpeed) {
		m_lastHit += dt;
		return;
	}
	m_lastHit -= m_data->hitSpeed;
	if (target == nullptr || target->isDeath()) {
		target = Container::shared()->getInRange(getPosition(), m_data->range);
	}

	if (target != nullptr) {
		if (getPosition().getDistance(target->getPosition()) > m_data->range) {
			target = nullptr;
		} else {
			Factory::createWeapon(this);
		}
	}
}

int Tower::getRange() {
	return m_data->range + m_data->range * m_level / 4;
}

TowerId Tower::getId() {
	return m_data->tag;
}

Enemy* Tower::getTarget() {
	return target;
}

void Tower::upgrade() {
	m_level++;
}

int Tower::upgradePrice() {
	return m_data->price *(m_level + 1);
}

int Tower::destroyPrice() {
	return m_data->price *m_level/2;
}