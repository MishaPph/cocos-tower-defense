#ifndef __BASE_ENEMY_H__
#define __BASE_ENEMY_H__

#pragma once
#include "cocos2d.h"
#include <list>
#include "IntPair.h"
#include "Types.h"

class Enemy;
typedef void (cocos2d::CCObject::*SEL_CallFuncEnemy)(Enemy*);

class Enemy : public cocos2d::CCSprite {
protected:
	int m_level, hp;
	std::list<IntPair> path;
	cocos2d::CCSprite* m_lifebar;
	SEL_CallFuncEnemy m_callFunc;
	cocos2d::CCObject* m_pListener;
	bool m_finish;
	float m_freeze;
public:
	Enemy();
	~Enemy();
	static Enemy* create(EnemyData * data);
	void setCallback(cocos2d::CCObject* target, SEL_CallFuncEnemy selector);
	void damaged(int value);
	EnemyData * getData();
	void callDeath();
	void freeze(float time);
	bool isDeath();
	bool isFinish();
	virtual bool init(void);
	virtual void onEnter() override;
	virtual void onExit() override;
	virtual void update(float dt) override;
};
#endif