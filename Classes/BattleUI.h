#ifndef __BATTLE_UI_H__
#define __BATTLE_UI_H__

#include "cocos2d.h"
#include "Types.h"

class BattleUI : public cocos2d::CCLayer
{
	cocos2d::CCMenu* m_GroundMenu;
	cocos2d::CCMenu* m_ChageMenu;
	cocos2d::CCLabelTTF* m_MoneyLabel;
	cocos2d::CCLabelTTF* m_DeathLabel;

	cocos2d::CCMenu* m_PauseMenu;

	void createPause();
	void createGroupTowers();
	void createChangeTower();
public:
	CCObject* m_scene;
	CREATE_FUNC(BattleUI);
	// on "init" you need to initialize your instance
	virtual bool init();

	virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);

	cocos2d::CCMenuItemImage* getTowerIcon(TowerData &towerData);

	void showUpgrade(const cocos2d::CCPoint &position, int cost, bool available);
	void showCreateTower(const cocos2d::CCPoint &position, int money);

	void changePause(CCObject* pSender);
	void showEnd(bool win);
	void setFinishEnemy(int count);
	void setMoney(int count);
	void setMoney(const cocos2d::CCPoint &from, int count);
	void finishCoins(CCNode*);
#pragma region CallBack
	void createTower(cocos2d::CCObject* pSender);
	void upgradeTower(cocos2d::CCObject* pSender);
	void destroyTower(cocos2d::CCObject* pSender);
#pragma endregion
};

#endif