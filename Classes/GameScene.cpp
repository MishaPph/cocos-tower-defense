#include "GameScene.h"
#include "AppMacros.h"
#include "TowerPoint.h"
#include "Map.h"
#include "BattleUI.h"
#include "Container.h"
#include "Wave.h"

USING_NS_CC;

CCScene* GameScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // 'layer' is an autorelease object
    GameScene *layer = GameScene::create();
    // add layer as a child to scene
    scene->addChild(layer, 0);
	scene->addChild(layer->m_ui, 0);
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init())
    {
        return false;
    }

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	Map::sharedMap()->loadMap("map_1.png");

	auto data = Map::sharedMap()->getTowerPoints();
	for (auto it = data.begin(); it != data.end(); ++it) {
		addTowerPoint(*it);
	}
	m_ui = BattleUI::create();
	m_ui->m_scene = this;
	m_Money = 0;
	m_enemyCount = 12;

	addMoney(1000);
	m_ui->setFinishEnemy(m_enemyCount);

	CCSprite * bg = CCSprite::create("backgroud.jpg");
	addChild(bg, -1);
	bg->setPosition(ccp(visibleSize.width /2 + origin.x/2, visibleSize.height /2 - origin.y));
	bg->setScaleX((visibleSize.width + origin.x) / bg->getContentSize().width);
	bg->setScaleY(visibleSize.height / bg->getContentSize().height);
    return true;
}

void GameScene::onEnter() {
	CCLayer::onEnter();
	Wave* wave = Wave::create(this);
	addChild(wave);
}

void GameScene::addTowerPoint(cocos2d::CCPoint &point) {
	TowerPoint *towerPoint = TowerPoint::create(CCSprite::create("tower_point.png"), this, menu_selector(GameScene::pointClickCallback));
	towerPoint->setPosition(point);
	CCMenu* pMenu = CCMenu::create(towerPoint, NULL);
	pMenu->setPosition(CCPointZero);
	this->addChild(pMenu, 1);
}

void GameScene::createTower(CCObject* pSender) {
	auto item = (CCMenuItem*)pSender;
	TowerData* data = (TowerData*)item->getUserData();
	auto tower = Tower::create(data);
	addChild(tower, 2);
	tower->setPosition(m_activePoint->getPosition());
	addMoney(-data->price);
	m_activePoint->setTower(tower);
}

void GameScene::upgradeTower(CCObject* pSender) {
	addMoney(-m_activePoint->getTower()->upgradePrice());
	m_activePoint->getTower()->upgrade();
}

void GameScene::destroyTower(CCObject* pSender) {
	addMoney(m_activePoint->getTower()->destroyPrice());
	m_activePoint->setTower(nullptr);
}

void GameScene::pointClickCallback(CCObject* pSender) {
	m_activePoint = ((TowerPoint *)pSender);
	if (m_activePoint->haveTower()) // show upgrade
	{
		auto t = m_activePoint->getTower()->upgradePrice();
		auto pos = m_activePoint->getPosition();
		m_ui->showUpgrade(pos, t, canBuy(m_activePoint->getTower()->upgradePrice()));
	} else {
		m_ui->showCreateTower(m_activePoint->getPosition(), m_Money);
	}
}

void GameScene::addEnemyFinish(const cocos2d::CCPoint &from) {
	m_enemyCount--;
	m_ui->setFinishEnemy(m_enemyCount);
}

#pragma region Money
bool GameScene::canBuy(int count) {
	return m_Money >= count;
}

void GameScene::addMoney(int count) {
	m_Money += count;
	m_ui->setMoney(m_Money);
}

void GameScene::addMoney(const CCPoint& from, int count) {
	m_Money += count;
	m_ui->setMoney(from, m_Money);
}
#pragma endregion

void GameScene::menuCloseCallback(CCObject* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
#else
    CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
#endif
}
