#ifndef __TOWER_POINT_H__
#define __TOWER_POINT_H__

#include "cocos2d.h"
#include "Tower.h"

USING_NS_CC;

class TowerPoint : public CCMenuItem
{
protected:
	Tower* m_Tower;
	CCNode* m_node;
public:
	static TowerPoint* create(CCNode *rec, CCObject* target, SEL_MenuHandler selector);
	void setTower(Tower* tower) {
		if (m_Tower) {
			m_Tower->removeFromParentAndCleanup(true);
		}
		m_Tower = tower;
		m_node->setVisible(!m_Tower);
	}

	Tower* getTower() {
		return m_Tower;
	}

	bool haveTower() {
		return m_Tower != nullptr;
	}
};

#endif // __TOWER_POINT_H__